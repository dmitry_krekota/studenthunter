(function () {

    angular.module('users').controller('StudentsLocationController', ['$scope', 'LocationsService',
        function ($scope, LocationsService) {
            var options, map;

            LocationsService.getLocations(function (data) {
                var i, markers = {};

                for (i = 0; i < data.length; i++) {
                    if (!markers[data[i].student_name] || markers[data[i].student_name].created < data[i].created) {
                        markers[data[i].student_name] = {
                            latitude: data[i].latitude,
                            longitude: data[i].longitude,
                            created: data[i].created,
                            accuracy: data[i].accuracy
                        }
                    }
                }
                for (i in markers) {
                    if (markers.hasOwnProperty(i)) {
                        addMarker(markers[i].latitude, markers[i].longitude, markers[i].accuracy, i + ": " + markers[i].created);
                    }
                }
            });

            function addMarker(latitude, longitude, accuracy, content) {
                var marker;

                if (!map) {
                    options = {
                        zoom: 9,
                        center: new google.maps.LatLng(latitude, longitude), // centered US
                        mapTypeId: google.maps.MapTypeId.TERRAIN,
                        mapTypeControl: false
                    };
                    map = new google.maps.Map(document.getElementById('map_canvas2'), options);
                }

                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: './point.png'
                });

                new google.maps.Circle({
                    center: new google.maps.LatLng(latitude, longitude),
                    radius: parseInt(accuracy),
                    map: map,
                    fillColor: '#f2d382',
                    fillOpacity: 0.3,
                    strokeColor: '#f2d382',
                    strokeOpacity: 0.8
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infowindow.open(map, marker);
                });
            }

        }
    ]);
})();