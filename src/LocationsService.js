angular.module('users').service('LocationsService', ["$http", function ($http) {
    var self = this;

    self.getLocations = function (callback) {
        $http({
            url: "./api/get_locations/",
            method: "GET"
        }).success(function (data) {
            callback(data);
        });
    };

    self.sendLocation = function (params, callback) {
        $http({
            url: "./api/send_location/",
            method: "POST",
            transformRequest: function (request) {
                var stringRequest = [], i;

                for (i in request) {
                    if (request.hasOwnProperty(i)) {
                        stringRequest.push(encodeURIComponent(i) + "=" + encodeURIComponent(request[i]));
                    }
                }
                return stringRequest.join("&");
            },
            data: {
                username: params.username,
                latitude: params.latitude,
                longitude: params.longitude,
                accuracy: params.accuracy
            },
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }).success(function (data) {
            callback(data);
        });
    };

    self.getTemperature = function () {
        return -3 + Math.round(Math.random())- Math.round(Math.random())+ Math.round(Math.random())- Math.round(Math.random());
    };

    self.getWindSpeed = function () {
        return 8 + Math.round(Math.random())- Math.round(Math.random())+ Math.round(Math.random())- Math.round(Math.random());
    };

    self.getHumidity = function () {
        return 80 + Math.round(Math.random())- Math.round(Math.random())+ Math.round(Math.random())- Math.round(Math.random());
    };

    self.getProbabilityOfPrecipitation = function () {
        return 8 + Math.round(Math.random())- Math.round(Math.random())+ Math.round(Math.random())- Math.round(Math.random());
    };


    self.inscribedCircleInTriangle = function (lat1, lon1, lat2, lon2, lat3, lon3) {
        var center, radius, dist1, dist2, dist3;

        lat1 = parseFloat(lat1);
        lon1 = parseFloat(lon1);
        lat2 = parseFloat(lat2);
        lon2 = parseFloat(lon2);
        lat3 = parseFloat(lat3);
        lon3 = parseFloat(lon3);

        dist1 = calculate_dist(lat1, lon1, lat2, lon2);
        dist2 = calculate_dist(lat2, lon2, lat3, lon3);
        dist3 = calculate_dist(lat3, lon3, lat1, lon1);
        center = getLatLngCenter([[lat1, lon1], [lat2, lon2], [lat3, lon3]], [dist1, dist2, dist3]);
        radius = calc_radius([dist1, dist2, dist3]);
        return {
            latitude: center[0],
            longitude: center[1],
            radius: radius
        }
    };

    //Distance between 2 points
    function calculate_dist(lat1, lon1, lat2, lon2) {
        var r = 6371000; //Earth radius
        var f1 = degr2rad(lat1);
        var f2 = degr2rad(lat1);
        var det_f = degr2rad((lat2 - lat1));
        var det_l = degr2rad((lon2 - lon1));
        var a = Math.pow(Math.sin(det_f / 2), 2) + Math.cos(f1) * Math.cos(f2) * Math.pow(Math.sin(det_l / 2), 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return r * c;
    }

    //latLngInDegr i array of 3 arrays with coords, ex:[[lat1,lon1],[lat2,lon2],[lat3,lon3]]
    //distInMet = array of 3 distances, ex:[dist1, dist2, dist3]
    function getLatLngCenter(latLngInDegr, distInMet) {
        var LATIDX = 0;
        var LNGIDX = 1;
        var X = [];
        var Y = [];
        var Z = [];
        for (var i = 0; i < latLngInDegr.length; i++) {
            var lat = degr2rad(latLngInDegr[i][LATIDX]);
            var lng = degr2rad(latLngInDegr[i][LNGIDX]);
            X[i] = Math.cos(lat) * Math.cos(lng);
            Y[i] = Math.cos(lat) * Math.sin(lng);
            Z[i] = Math.sin(lat);
        }
        var X_out = (X[0] * distInMet[1] + X[1] * distInMet[2] + X[2] * distInMet[0]) / (distInMet[0] + distInMet[1] + distInMet[2]);
        var Y_out = (Y[0] * distInMet[1] + Y[1] * distInMet[2] + Y[2] * distInMet[0]) / (distInMet[0] + distInMet[1] + distInMet[2]);
        var Z_out = (Z[0] * distInMet[1] + Z[1] * distInMet[2] + Z[2] * distInMet[0]) / (distInMet[0] + distInMet[1] + distInMet[2]);

        lng = Math.atan2(Y_out, X_out);
        var hyp = Math.sqrt(X_out * X_out + Y_out * Y_out);
        lat = Math.atan2(Z_out, hyp);

        return ([rad2degr(lat), rad2degr(lng)]);
    }

    // distances = 3 array of 3 numbers
    function calc_radius(distances) {
        var p = (distances[0] + distances[1] + distances[2]) / 2;
        return Math.sqrt(((p - distances[0]) * (p - distances[1]) * (p - distances[2])) / p);
    }

    function rad2degr(rad) {
        return rad * 180 / Math.PI;
    }

    function degr2rad(degr) {
        return degr * Math.PI / 180;
    }

}]);
