(function () {

    angular.module('users').controller('TrilaterationController', ['$scope', 'LocationsService',
        function ($scope, LocationsService) {
            var allPoints, options, map;

            function sortArrayByProperty(array, property) {
                return array.sort(function compare(a, b) {
                    if (a[property] < b[property])
                        return -1;
                    if (a[property] > b[property])
                        return 1;
                    return 0;
                });
            }

            $scope.students = [];
            $scope.selectedStudentPoints = [];

            LocationsService.getLocations(function (data) {
                var i, students = {};

                allPoints = data;

                for (i = 0; i < data.length; i++) {
                    if (jQuery.inArray(data[i].student_name, $scope.students) == -1) {
                        $scope.students.push(data[i].student_name);
                    }
                }

                $scope.selectedStudent = $scope.students[0];
                $scope.selectedStudentChanged();
            });

            $scope.selectedStudentChanged = function () {
                var i;

                $scope.selectedStudentPoints = [];

                for (i = 0; i < allPoints.length; i++) {
                    if (allPoints[i].student_name == $scope.selectedStudent) {
                        $scope.selectedStudentPoints.push(allPoints[i]);
                    }
                }

                sortArrayByProperty($scope.selectedStudentPoints, "created");
                drawTwoLastPoints();
                drawNextLineOfSelectedStudent();
                drawLastPointsOfStudents();
                drawLinesOfStudents();
            };


            function drawNextLineOfSelectedStudent() {
                var studentPoints, i, j, k, circleCoordinates,
                    lastPointOfSelectedStudent,
                    circleWithMaxRadiusCoordinates, resultPoints = [];

                for (j = 0; j < $scope.students.length; j++) {
                    studentPoints = [];
                    for (i = 0; i < allPoints.length; i++) {
                        if (allPoints[i].student_name == $scope.students[j] && allPoints[i].student_name != $scope.selectedStudent) {
                            studentPoints.push(allPoints[i]);
                        }
                    }
                    sortArrayByProperty(studentPoints, "created");
                    if (studentPoints[studentPoints.length - 1]) {
                        resultPoints.push(studentPoints[studentPoints.length - 1]);
                    }
                }

                for (j = 0; j < resultPoints.length; j++) {
                    for (i = j; i < resultPoints.length; i++) {
                        for (k = i; k < resultPoints.length; k++) {
                            if (resultPoints[i] != resultPoints[j] && resultPoints[j] != resultPoints[k] && resultPoints[k] != resultPoints[i]) {
                                circleCoordinates = LocationsService.inscribedCircleInTriangle(resultPoints[i].latitude, resultPoints[i].longitude, resultPoints[j].latitude, resultPoints[j].longitude, resultPoints[k].latitude, resultPoints[k].longitude);
                                if ((!circleWithMaxRadiusCoordinates && !isNaN(circleCoordinates.radius))
                                    || (circleWithMaxRadiusCoordinates && !isNaN(circleCoordinates.radius) && circleWithMaxRadiusCoordinates.radius < circleCoordinates.radius)) {
                                    circleWithMaxRadiusCoordinates = circleCoordinates;
                                }
                            }
                        }
                    }
                }

                lastPointOfSelectedStudent = $scope.selectedStudentPoints[$scope.selectedStudentPoints.length - 1];
                if (lastPointOfSelectedStudent && circleWithMaxRadiusCoordinates) {
                    drawLineBeatenPoints(lastPointOfSelectedStudent, circleWithMaxRadiusCoordinates, "#a41c1c", 4, 2000);
                }
            }

            function drawTwoLastPoints() {
                var point1 = $scope.selectedStudentPoints[$scope.selectedStudentPoints.length - 1],
                    point2 = $scope.selectedStudentPoints[$scope.selectedStudentPoints.length - 2];

                if (point1) {
                    addMarker(point1.latitude, point1.longitude, point1.accuracy, $scope.selectedStudent + ": last place", true);
                    if (point2) {
                        addMarker(point2.latitude, point2.longitude, point2.accuracy, $scope.selectedStudent + ": previous place");
                        drawLineBeatenPoints(point1, point2,'#0637d2', 4, 2000);
                    }
                }
            }

            function drawLastPointsOfStudents() {
                var studentPoints, i, j, point;

                for (j = 0; j < $scope.students.length; j++) {
                    studentPoints = [];
                    for (i = 0; i < allPoints.length; i++) {
                        if (allPoints[i].student_name == $scope.students[j] && allPoints[i].student_name != $scope.selectedStudent) {
                            studentPoints.push(allPoints[i]);
                        }
                    }
                    sortArrayByProperty(studentPoints, "created");
                    point = studentPoints[studentPoints.length - 1];
                    if (point) {
                        addMarker(point.latitude, point.longitude, point.accuracy, $scope.students[j] + ": last place");
                    }
                }
            }

            function drawLinesOfStudents() {
                var studentPoints, i, j, k, circleCoordinates, resultPoints = [];

                for (j = 0; j < $scope.students.length; j++) {
                    studentPoints = [];
                    for (i = 0; i < allPoints.length; i++) {
                        if (allPoints[i].student_name == $scope.students[j] && allPoints[i].student_name != $scope.selectedStudent) {
                            studentPoints.push(allPoints[i]);
                        }
                    }
                    sortArrayByProperty(studentPoints, "created");
                    if (studentPoints[studentPoints.length - 1]) {
                        resultPoints.push(studentPoints[studentPoints.length - 1]);
                    }
                }

                for (j = 0; j < resultPoints.length; j++) {
                    for (i = 0; i < resultPoints.length; i++) {
                        drawLineBeatenPoints(resultPoints[i], resultPoints[j], "#6bb957");
                    }
                }

                for (j = 0; j < resultPoints.length; j++) {
                    for (i = j; i < resultPoints.length; i++) {
                        for (k = i; k < resultPoints.length; k++) {
                            if (resultPoints[i] != resultPoints[j] && resultPoints[j] != resultPoints[k] && resultPoints[k] != resultPoints[i]) {
                                circleCoordinates = LocationsService.inscribedCircleInTriangle(resultPoints[i].latitude, resultPoints[i].longitude, resultPoints[j].latitude, resultPoints[j].longitude, resultPoints[k].latitude, resultPoints[k].longitude);
                                if(!isNaN(circleCoordinates.radius)){
                                    drawCircleInTriangle(circleCoordinates);
                                }
                            }
                        }
                    }
                }
            }

            function addMarker(latitude, longitude, accuracy, content, mapReset) {
                var marker;

                if (!map || mapReset) {
                    options = {
                        zoom: 9,
                        center: new google.maps.LatLng(latitude, longitude), // centered US
                        mapTypeId: google.maps.MapTypeId.TERRAIN,
                        mapTypeControl: false
                    };
                    map = new google.maps.Map(document.getElementById('map_canvas3'), options);
                }

                marker = new google.maps.Marker({
                    map: map,
                    position: new google.maps.LatLng(latitude, longitude),
                    icon: './point.png'
                });

                new google.maps.Circle({
                    center: new google.maps.LatLng(latitude, longitude),
                    radius: parseFloat(accuracy),
                    map: map,
                    fillColor: '#bcb61f',
                    fillOpacity: 0.3,
                    strokeColor: '#bcb61f',
                    strokeOpacity: 0.8
                });

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow = new google.maps.InfoWindow({
                        content: content
                    });
                    infowindow.open(map, marker);
                });
            }

            function drawLineBeatenPoints(point1, point2, color, strokeWeight, zIndex) {
                var coordinates = [
                    new google.maps.LatLng(point1.latitude, point1.longitude),
                    new google.maps.LatLng(point2.latitude, point2.longitude)
                ];
                new google.maps.Polyline({
                    path: coordinates,
                    geodesic: true,
                    map: map,
                    strokeColor: color || '#FF0000',
                    strokeOpacity: 1.0,
                    zIndex: zIndex || 1000,
                    strokeWeight: strokeWeight || 2
                });
            }

            function drawCircleInTriangle(circleCoordinates) {
                new google.maps.Circle({
                    center: new google.maps.LatLng(circleCoordinates.latitude, circleCoordinates.longitude),
                    radius: circleCoordinates.radius,
                    map: map,
                    fillColor: '#1ea721',
                    fillOpacity: 0.3,
                    strokeColor: '#1ea721',
                    strokeOpacity: 0.8,
                    zIndex: 500
                });
            }

        }
    ]);

})();