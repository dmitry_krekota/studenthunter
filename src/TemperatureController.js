(function () {

    angular.module('users').controller('TemperatureController', ['$scope', '$mdToast', '$mdDialog', 'LocationsService',
        function (_$scope, $mdToast, $mdDialog, LocationsService) {
            var last = {
                bottom: false,
                top: true,
                left: false,
                right: true
            }, latitude, longitude, accuracy;

            _$scope.sendLocation = function (event) {
                if (_$scope.username) {
                    sendLocationToServer();
                } else {
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'dialog_t.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: event,
                        clickOutsideToClose: true
                    });
                }
            };

            function DialogController($scope, $rootScope, $mdDialog, $mdToast) {
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.submit = function () {
                    if (!$scope.name || !$scope.name.length) {
                        $mdToast.show(
                            $mdToast.simple()
                                .content('Enter temperature!')
                                .action('OK')
                                .theme('error')
                                .position(_$scope.getToastPosition())
                                .hideDelay(4000)
                        );
                    } else {
                        //$rootScope.username = $scope.name;
                        //localStorage.setItem('username', $rootScope.username);
                        $mdDialog.hide();
                        sendLocationToServer();
                    }
                };
            }

            function sendLocationToServer() {
                //if (_$scope.username && latitude && longitude) {
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Temperature was sent successfully!')
                            .action('OK')
                            .position(_$scope.getToastPosition())
                            .hideDelay(3000)
                    );
                //} else {
                //    $mdToast.show(
                //        $mdToast.simple()
                //            .content('Error!')
                //            .action('OK')
                //            .theme('error')
                //            .position(_$scope.getToastPosition())
                //            .hideDelay(3000)
                //    );
                //}
            }

            _$scope.toastPosition = angular.extend({}, last);
            _$scope.getToastPosition = function () {
                sanitizePosition();
                return Object.keys(_$scope.toastPosition)
                    .filter(function (pos) {
                        return _$scope.toastPosition[pos];
                    })
                    .join(' ');
            };
            function sanitizePosition() {
                var current = _$scope.toastPosition;
                if (current.bottom && last.top) current.top = false;
                if (current.top && last.bottom) current.bottom = false;
                if (current.right && last.left) current.left = false;
                if (current.left && last.right) current.right = false;
                last = angular.extend({}, current);
            }

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }

            function showPosition(position) {
                var options, map, marker;
                options = {
                    zoom: 9,
                    center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude), // centered US
                    mapTypeId: google.maps.MapTypeId.TERRAIN,
                    mapTypeControl: false
                };
                map = new google.maps.Map(document.getElementById('map_canvas4'), options);

                latitude = position.coords.latitude;
                longitude = position.coords.longitude;
                accuracy = position.coords.accuracy;

                new google.maps.Circle({
                    center: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                    radius: position.coords.accuracy,
                    map: map,
                    fillColor: '#f2d382',
                    fillOpacity: 0.3,
                    strokeColor: '#f2d382',
                    strokeOpacity: 0.8
                });
                var start = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                var end = new google.maps.LatLng(position.coords.latitude + 0.05, position.coords.longitude - 0.05);


                marker = new google.maps.Marker({
                    map: map,
                    position: start,
                    icon: './point.png'
                });


                var move_marker = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: new google.maps.LatLng(position.coords.latitude + 0.05, position.coords.longitude - 0.05)
                });

                var directionsDisplay = new google.maps.DirectionsRenderer();// also, constructor can get "DirectionsRendererOptions" object
                directionsDisplay.setMap(map); // map should be already initialized.

                var request = {
                    origin: start,
                    destination: end,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                var directionsService = new google.maps.DirectionsService();
                directionsService.route(request, function (response, status) {
                    if (status == google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    }
                });

                _$scope.temperature = LocationsService.getTemperature();
                _$scope.windSpeed = LocationsService.getWindSpeed();
                _$scope.humidity = LocationsService.getHumidity();
                _$scope.probabilityOfPrecipitation = LocationsService.getProbabilityOfPrecipitation();
                _$scope.$apply();

                google.maps.event.addListener(move_marker, 'dragend', function (data) {
                        var request = {
                            origin: start,
                            destination: data.latLng,
                            travelMode: google.maps.TravelMode.DRIVING
                        };
                        var directionsService = new google.maps.DirectionsService();
                        directionsService.route(request, function (response, status) {
                            if (status == google.maps.DirectionsStatus.OK) {
                                directionsDisplay.setDirections(response);
                            }
                        });

                        _$scope.temperature = LocationsService.getTemperature();
                        _$scope.windSpeed = LocationsService.getWindSpeed();
                        _$scope.humidity = LocationsService.getHumidity();
                        _$scope.probabilityOfPrecipitation = LocationsService.getProbabilityOfPrecipitation();
                        _$scope.$apply();
                }
                );


                function getAddressName(latLng, callback) {
                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                            "location": latLng
                        },
                        function (results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                callback(results[0].formatted_address);
                            } else {
                                callback(null);
                            }
                        });
                }

                (function (marker) {
                    getAddressName(new google.maps.LatLng(position.coords.latitude, position.coords.longitude), function (name) {
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow = new google.maps.InfoWindow({
                                content: (name) ? 'Your current location: ' + name : 'Your current location'
                            });
                            infowindow.open(map, marker);
                        });
                    });
                })(marker);
            }
        }
    ]);
})();