(function () {
    'use strict';
    angular.module('users').service('userService', ['$q', UserService]);
    function UserService($q) {
        var users = [
            {
                id: 1,
                name: 'Your location',
                avatar: 'svg-1'
            },
            {
                id: 2,
                name: 'Students\' location',
                avatar: 'svg-2'
            },
            {
                id: 3,
                name: 'Trilateration',
                avatar: 'svg-2'
            },
            {
                id: 4,
                name: 'Temperature',
                avatar: 'svg-2'
            }
        ];

        return {
            loadAllUsers: function () {
                return $q.when(users);
            }
        };
    }
})();
