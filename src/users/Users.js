(function () {
    'use strict';

    angular.module('users', ['ngMaterial']);

    angular.module('users').config(['$mdThemingProvider', function ($mdThemingProvider) {
        $mdThemingProvider.theme("error");
    }]);

    angular.module('users').run(['$rootScope', function ($rootScope) {
        $rootScope.username = localStorage.getItem('username') || null;
        $rootScope.logout = function(){
            $rootScope.username = null;
            localStorage.removeItem('username');
        }
    }]);

})();
