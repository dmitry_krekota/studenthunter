<?php

namespace StudentHunter\Table;

use PDO;
use StudentHunter\Database\Connection;

class Locations
{
    private $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    public function getLocations()
    {
        $sql = "SELECT `locations`.id, `locations`.latitude, `locations`.student_id,
                `locations`.longitude , `locations`.accuracy, `locations`.created, `students`.name AS student_name
                FROM `locations`
                INNER JOIN `students` ON `locations`.student_id = `students`.id";
        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        return $statement->fetchAll();
    }

    public function sendLocation($studentName, $latitude, $longitude, $accuracy)
    {
        $sql = "SELECT `students`.id FROM `students` WHERE `students`.name='$studentName'";

        $statement = $this->connection->prepare($sql);
        $statement->execute();
        $user = $statement->fetch();
        $now = date('Y-m-d H:i:s');

        if ($user) {
            $userId = $user['id'];
        } else {
            $sql = "INSERT INTO `students` (name, created) VALUES ('$studentName', '$now')";
            $statement = $this->connection->prepare($sql);
            $statement->execute();
            $userId = $this->connection->lastInsertId();
        }

        $sql = "INSERT INTO `locations` (student_id, latitude, longitude, accuracy, created)
            VALUES ('$userId', '$latitude', '$longitude', '$accuracy', '$now')";

        $statement = $this->connection->prepare($sql);
        $statement->execute();

    }

}