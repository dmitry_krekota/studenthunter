<?php

use StudentHunter\Table\Locations;

require 'vendor/autoload.php';

$app = new Slim\App();

$app->get('/get_locations/', function ($request, $response, $args) {
    $locationsTable = new Locations();
    $locations = $locationsTable->getLocations();
    $response->write(json_encode($locations));
    return $response;
});

$app->post('/send_location/', function ($request, $response, $args) {
    $locationsTable = new Locations();
    $locationsTable->sendLocation($_POST['username'],$_POST['latitude'],$_POST['longitude'],$_POST['accuracy']);
    $response->write(true);
    return $response;
});

$app->run();
