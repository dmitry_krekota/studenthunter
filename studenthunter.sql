-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 23, 2015 at 12:25 AM
-- Server version: 5.6.22-log
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `studenthunter`
--

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `latitude` varchar(100) NOT NULL,
  `longitude` varchar(100) NOT NULL,
  `accuracy` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `student_id`, `latitude`, `longitude`, `accuracy`, `created`) VALUES
(3, 3, '46.94182910030001', '32.0593387', '4', '2015-11-21 04:13:33'),
(4, 4, '46.9418291300001', '32.0543887', '5', '2015-11-21 02:13:33'),
(5, 1, '46.94182910000001', '32.041887', '2', '2015-10-21 04:16:33'),
(6, 2, '46.94282910066', '32.0531866', '3', '2015-12-21 04:16:33'),
(7, 3, '46.94382910030001', '32.0593387', '4', '2015-11-26 00:00:00'),
(8, 4, '46.9448291300001', '32.0543887', '5', '2015-07-21 02:13:33'),
(11, 1, '46.941842099999995', '32.0591634', '52', '2015-11-21 02:13:33'),
(12, 1, '46.94182970000001', '32.0591254', '50', '2015-11-23 00:13:31'),
(13, 1, '46.94182970000001', '32.0591254', '50', '2015-11-23 00:13:38'),
(14, 5, '46.9418188', '32.0590481', '46', '2015-11-23 00:17:24'),
(15, 5, '46.9418492', '32.0591301', '51', '2015-11-23 00:18:07'),
(16, 5, '46.9418492', '32.0591301', '51', '2015-11-23 00:18:18'),
(17, 7, '46.9418272', '32.059103', '34', '2015-11-23 00:21:39'),
(18, 7, '46.9418204', '32.0590848', '48', '2015-11-23 00:22:49'),
(19, 7, '46.9418284', '32.0591049', '49', '2015-11-23 00:23:33'),
(20, 7, '46.9418284', '32.0591049', '49', '2015-11-23 00:23:41'),
(21, 7, '46.9418449', '32.059108', '50', '2015-11-23 00:24:18'),
(22, 7, '46.941814', '32.059138499999996', '49', '2015-11-23 00:25:00');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE IF NOT EXISTS `students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `created`) VALUES
(1, 'User1', '2015-11-18 11:08:11'),
(2, 'User2', '2015-11-18 11:08:11'),
(3, 'User3', '2015-11-17 10:11:09'),
(4, 'User4', '2015-11-18 05:15:21'),
(5, 'fff', '2015-11-23 00:16:32'),
(6, 'ddddddddd', '2015-11-23 00:18:30'),
(7, 'Dmitry', '2015-11-23 00:21:39');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
